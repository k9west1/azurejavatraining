package com.opsgility.training.documentdb.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.Gson;
import com.opsgility.training.documentdb.dao.DocDbDao;
import com.opsgility.training.documentdb.model.TodoItem;


@Controller
public class TodoController {
	@RequestMapping(value = "/todos", method = { RequestMethod.GET, RequestMethod.POST })
	public String getHello(ModelMap model, HttpServletRequest httpRequest) {
		return "/document/index";
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/todos/createTodoItem")
	@ResponseBody
	public String createTodoItem(@RequestParam(value = "todoItemName") String name,
			@RequestParam(value = "todoItemCategory") String category,
			@RequestParam(value = "todoItemComplete") boolean isComplete) {
		DocDbDao todoDao = new DocDbDao();
		TodoItem todoItem = new TodoItem(category, isComplete, null, name);
		Gson gson = new Gson();
		return gson.toJson(todoDao.createTodoItem(todoItem));
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/todos/getTodoItems")
	@ResponseBody
	public String getTodoItems() {
		DocDbDao todoDao = new DocDbDao();
		Gson gson = new Gson();
		return gson.toJson(todoDao.readTodoItems());
	}

	@RequestMapping(method = RequestMethod.POST, value = "/api/todos/updateTodoItem")
	@ResponseBody
	public String updateTodoItem(@RequestParam(value = "todoItemId") String id,
			@RequestParam(value = "todoItemComplete") boolean isComplete) {
		DocDbDao todoDao = new DocDbDao();
		Gson gson = new Gson();
		return gson.toJson(todoDao.updateTodoItem(id, isComplete));
	}
}