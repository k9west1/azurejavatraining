package com.opsgility.training.documentdb.dao;

import com.microsoft.azure.documentdb.*;

public class DocumentClientFactory {
private static final String HOST = "https://ops-todoitems-778.documents.azure.com:443/";
private static final String MASTER_KEY = "F7aU4wGH2Y5QM5PyoHBEOQ7uwOTlwWIarfJzjFIcmB7enL6nTksyc89QEcoZ6RrYP06VDBYJYLP1o8nPPKOFzw==";
private static DocumentClient documentClient;
public static DocumentClient getDocumentClient() {
 if (documentClient == null) {
 documentClient = new DocumentClient(HOST, MASTER_KEY,
 ConnectionPolicy.GetDefault(), ConsistencyLevel.Session);
 }
 return documentClient;
}
}
